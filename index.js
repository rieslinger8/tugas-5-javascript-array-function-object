// soal 1

// buatlah variabel seperti di bawah ini
// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// jawaban soal 1 

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan.sort()[i]);
};

// soal 2

// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

//  Tulis kode function di sini

// jawaban soal 2

var data = {
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby: "Gaming",
    introduce: function() {
        return 'Nama Saya ' + this.name + ' umur saya ' + this.age + ' ,tahun, alamat saya di ' + this.address + ', dan saya punya hobi yaitu ' + this.hobby;
    }
};
data.introduce();
console.log(data.introduce())

// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// soal no 3

// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

// var hitung_1 = hitung_huruf_vokal("Muhammad")

// var hitung_2 = hitung_huruf_vokal("Iqbal")

// console.log(hitung_1 , hitung_2) // 3 2

// Jawaban soal no 3
function hitung_huruf_vokal(str) {
    var hitung = 0;
    var hurufVokal = 'aeiou';

    for (var i = 0; i < str.length; i++) {
        if (hurufVokal.indexOf(str[i]) > -1) {
            hitung++;
        }
    }
    return hitung;
};

var muhammad = hitung_huruf_vokal('Muhammad');
console.log(muhammad);


// Soal 4
// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

// console.log( hitung(0) ) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8

// jawaban soal 4

function hitung(a) {
    for (var i = -2; i < a; i++) {
        if (a <= 0) {
            return a = -2
        } else if (a >= 1) {
            return a *= 2
        }

    };
};
console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))